package  
{

	public class MarineMemento 
	{
		public var x:Number;
		public var y:Number;
		
		public function MarineMemento(x:Number, y:Number) 
		{
			this.x = x;
			this.y = y;
		}
		
	}

}