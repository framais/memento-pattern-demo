package  
{

	public class MarineRecorder 
	{
		
		private var mementoIndex:uint = 0;
		private var mementos:Array = [];
		private var marine:Marine;
		
		public function MarineRecorder(marine:Marine) 
		{
			this.marine = marine;
		}
		
		public function play():void 
		{
			if (mementoIndex < mementos.length - 1)
			{
				mementoIndex++;
				
				// Get memento from Array
				marine.memento = mementos[mementoIndex];
			}
			
		}
		
		public function rewind():void 
		{
			if (mementoIndex > 0)
			{
				mementoIndex--;
				
				// Get memento from array
				marine.memento = mementos[mementoIndex];
			}
		}
		
		public function record():void 
		{
			// Store memento in array
			mementos.push(marine.memento);
		}
		
		public function reset():void 
		{
			mementos = [];
		}	
		
		public function stop():void 
		{
			mementoIndex = mementos.length;
		}		
	}

}